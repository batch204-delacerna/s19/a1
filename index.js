console.log("Hello World!");
//3. global variables
let username;
let password;
let role;

// 4. 
function userLogin() 
{
	username = prompt("Enter your username: ");
	password = prompt("Enter your password: ");
	role = prompt("Enter your role: ").toLowerCase();
	

	if ((username === null || username === "") ||
	    (password === null || password === "") ||
		(role === null || role === "")
	   )
	{
		alert("Input should not be empty!");
		userLogin() 
	}
	else
	{
		switch(role)
		{
			case 'admin':
				console.log("Welcome back to the class portal, admin!");
				break;
			case 'teacher':
				console.log("Thank you for logging in, teacher!");
				break;
			case 'student':
				console.log("Welcome to the class portal, student!");
				break;
			default:
				console.log("Role out of range.");
			break;
		}
	}

}

userLogin();




//5.

function checkAverage(grade1, grade2, grade3, grade4) 
{
	//scoped variable
	let average = (grade1 + grade2 + grade3 + grade4) /4;
	
    average = Math.round(average);
	console.log(average);

	if (average <= 74) 
	{
		console.log("Hello, student, your average is: "+ average +". The letter equivalent is F");
	}
	else if (average >= 75 && average <= 79) {
		console.log("Hello, student, your average is: "+ average +". The letter equivalent is D");
	}
	else if (average >= 80 && average <= 84) {
		console.log("Hello, student, your average is: "+ average +". The letter equivalent is C");
	}
	else if (average >= 85 && average <= 89) {
		console.log("Hello, student, your average is: "+ average +". The letter equivalent is B");
	}
	else if (average >= 90 && average <= 95){
		console.log("Hello, student, your average is: "+ average +". The letter equivalent is A");
	}
	else {
		console.log("Hello, student, your average is: "+ average +". The letter equivalent is A+");
	}
}

checkAverage(73,73,73,73);
checkAverage(75,75,75,75);
checkAverage(80,80,80,80);
checkAverage(85,85,85,85);
checkAverage(90,90,90,90);
checkAverage(96,96,96,96);